Module Module1
    Dim promptMessage As String = "Enter the name of an MOVE script to run or 'QUIT' to end this application."
    Dim sXML As String = "MoveSettings.xml"
    Dim iScripts As Integer = 0
    Dim ds As New DataSet

    Dim sName As String = ""
    Dim sEmailAddresses As String = ""
    Dim sUser As String = ""
    Dim sOriginFolder As String = ""
    Dim sDestFolder As String = ""
    Dim sProcessedFolder As String = ""
    Dim sMatch As String = ""
    Dim bCase As Boolean = False
    Dim sLog As String = ""

    Dim iCount As Integer = 0

    Dim sLogText As String = ""
    Dim sFileName As String

    Dim xTest As System.Diagnostics.Process
    Dim iTest As Integer
    Dim sMapDriveLetter As String = ""
    Dim sMapDrivePath As String = ""
    Dim sMapDriveUser As String = ""
    Dim sMapDrivePass As String = ""
    Dim bMapped As Boolean = False
    Dim bMapDrive As Boolean = False

    Sub Main()
        Dim Args As String() = System.Environment.GetCommandLineArgs()
        Dim strInput As String

        Try
            ReadXML(sXML)
        Catch ex As Exception
            WriteToLog("The settings cannot be read - " & ex.Message)
            ErrorAlert("Reading settings FAILED.  Error: " & ex.Message)
        End Try

        Console.WriteLine()
        Console.WriteLine(promptMessage)

        If Args.Length > 1 Then
            strInput = LCase(Args(1))

        Else
            strInput = LCase(Console.ReadLine())
        End If

        While strInput <> "quit"
            While iCount < iScripts
                Try
                    If ds.Tables("Script").Rows(iCount)("Name").ToString.ToLower = strInput Then
                        InitializeVars()
                        If bMapDrive = True Then
                            Try
                                If MapDrive() = False Then
                                    End
                                End If
                            Catch ex As Exception
                                WriteToLog("The drive cannot be mapped - " & ex.Message)
                                ErrorAlert("Mapping drive FAILED.  Error: " & ex.Message)
                            End Try
                        End If
                        Try
                            MoveFiles()
                        Catch ex As Exception
                            WriteToLog("The files cannot be moved - " & ex.Message)
                            ErrorAlert("Moving file FAILED.  Error: " & ex.Message)
                        End Try
                        Try
                            WriteLog()
                        Catch ex As Exception
                        End Try
                        End
                    End If
                Catch ex As Exception
                End Try
                iCount = iCount + 1
            End While
        End While
        End
    End Sub

    Sub ReadXML(ByVal sXMLFile As String)
        ds.ReadXml(System.IO.Path.GetFullPath(sXMLFile))
        iScripts = ds.Tables("Script").Rows.Count
    End Sub

    Sub InitializeVars()
        sName = ds.Tables("Script").Rows(iCount)("Name")
        sEmailAddresses = ds.Tables("Script").Rows(iCount)("EmailAddresses")
        sUser = ds.Tables("Script").Rows(iCount)("Name")
        sOriginFolder = ds.Tables("Script").Rows(iCount)("OriginDir")
        sDestFolder = ds.Tables("Script").Rows(iCount)("DestDir")
        sProcessedFolder = ds.Tables("Script").Rows(iCount)("ProcessedDir")
        sMatch = ds.Tables("Script").Rows(iCount)("Match")
        bCase = CBool(ds.Tables("Script").Rows(iCount)("CaseSensitive"))
        sLog = ds.Tables("Script").Rows(iCount)("Log")
        bMapDrive = CBool(ds.Tables("Script").Rows(iCount)("MapDrive"))
        sMapDriveLetter = ds.Tables("Script").Rows(iCount)("MapDriveLetter")
        sMapDrivePath = ds.Tables("Script").Rows(iCount)("MapDrivePath")
        sMapDriveUser = ds.Tables("Script").Rows(iCount)("MapDriveUser")
        sMapDrivePass = ds.Tables("Script").Rows(iCount)("MapDrivePass")

    End Sub
    Sub MoveFiles()
        Dim bFound As Boolean = False
        Dim sFiles() As String = IO.Directory.GetFiles(sOriginFolder)

        If sMatch.StartsWith("*") And sMatch.Length > 1 Then
            sMatch = sMatch.Replace("*", "")
            For Each file As String In sFiles
                If bCase = False Then
                    If IO.Path.GetFileName(file).ToLower.EndsWith(sMatch.ToLower) Then
                        MoveFile(file)
                        bFound = True
                    End If
                Else
                    If IO.Path.GetFileName(file).EndsWith(sMatch) Then
                        MoveFile(file)
                        bFound = True
                    End If
                End If
            Next
        ElseIf sMatch.EndsWith("*") And sMatch.Length > 1 Then
            sMatch = sMatch.Replace("*", "")
            For Each file As String In sFiles
                If bCase = False Then
                    If IO.Path.GetFileName(file).ToLower.StartsWith(sMatch.ToLower) Then
                        MoveFile(file)
                        bFound = True
                    End If
                Else
                    If IO.Path.GetFileName(file).StartsWith(sMatch) Then
                        MoveFile(file)
                        bFound = True
                    End If
                End If
            Next
        ElseIf sMatch = "*" Then
            For Each file As String In sFiles
                MoveFile(file)
                bFound = True
            Next
        Else
            For Each file As String In sFiles
                If bCase = False Then
                    If IO.Path.GetFileName(file).ToLower = sMatch.ToLower Then
                        MoveFile(file)
                        bFound = True
                    End If
                Else
                    If IO.Path.GetFileName(file) = sMatch Then
                        MoveFile(file)
                        bFound = True
                    End If
                End If

            Next
        End If

        If bFound = False Then
            WriteToLog(sName & " - script skipped.  No files to send")
            End
        End If
    End Sub

    Sub MoveFile(ByVal sOrigFilePath As String)
        sFileName = IO.Path.GetFileName(sOrigFilePath)
        Try
            IO.File.Move(sOrigFilePath, sDestFolder & "\" & sFileName)
            WriteToLog("Moved file: " & sOrigFilePath & " to " & sDestFolder)
            If sProcessedFolder <> "" Then
                CopyToProcessed(sDestFolder & "\" & sFileName)
            End If
        Catch ex As Exception
            WriteToLog("Move file " & sOrigFilePath & " FAILED.")
            ErrorAlert("Move file " & sOrigFilePath & " to " & sDestFolder & " FAILED.  Error: " & ex.Message)
        End Try
    End Sub

    Sub CopyToProcessed(ByVal sOrigPath As String)
        Try
            IO.File.Copy(sOrigPath, sProcessedFolder & "\" & sFileName)
            WriteToLog("Copied file: " & sOrigPath & " to " & sProcessedFolder)
        Catch ex As Exception
            WriteToLog("Copy file " & sOrigPath & " FAILED.")
            ErrorAlert("Copy file " & sOrigPath & " to " & sProcessedFolder & "\" & sFileName & " FAILED.  Error: " & ex.Message)
        End Try
    End Sub

    Sub WriteToLog(ByVal sMessage As String)
        Dim sTime As String = Now.TimeOfDay.ToString
        Try
            If sTime.Length > 8 Then
                sTime.Remove(8)
            End If
            sLogText = sLogText & vbNewLine & Now.Date.ToShortDateString & " " & sTime & " " & sMessage
        Catch ex As Exception
            sLogText = sLogText & vbNewLine & Now & " " & sMessage
        End Try

    End Sub

    Sub WriteLog()
        Dim sw As New IO.StreamWriter(sLog, True)
        sw.WriteLine("*****************************************************************************")
        sw.WriteLine("MOVE FILES SCRIPT RUNNING..........................")
        sw.Write(sLogText)
        sw.WriteLine(vbNewLine)
        sw.WriteLine("..........................MOVE FILES SCRIPT CLOSING")
        sw.WriteLine("*****************************************************************************")
        sw.Close()
    End Sub

    Sub ErrorAlert(ByVal sError As String) ', 
        Dim ws As New com.pilotair.ws.EmailWS()
        ws.SendEmail("WebRegMail", sEmailAddresses, Environment.MachineName & "@pilotair.com", _
                    "Move File Error Alert", "The " & sName & " Move process returned the following error: " & sError & _
                    ". " & Now) ', mmann@pilotdelivers.com, jcameron@pilotdelivers.com, cweiss@pilotdelivers.com
        ws = Nothing
    End Sub


    Function MapDrive() As Boolean
        Try
            xTest = System.Diagnostics.Process.Start("net", "use " & sMapDriveLetter & ":\ /delete")
            iTest = 0
            Do While iTest < 10000 And Not xTest.HasExited
                'Do While Not xTest.HasExited
                iTest = iTest + 1
            Loop
            If xTest.HasExited Then
                xTest = System.Diagnostics.Process.Start("net", "use " & sMapDriveLetter & ": " & sMapDrivePath & " /user:" & sMapDriveUser & " " & sMapDrivePass)
                iTest = 0
                'Do While iTest < 100 And Not xTest.HasExited
                Do While Not xTest.HasExited
                    iTest = iTest + 1
                Loop
                If xTest.HasExited Then
                    bMapped = True
                End If
            End If
        Catch ex As Exception
            WriteToLog(ex.Message)
        End Try

        If bMapped = False Then
            WriteToLog("The drive cannot be mapped")
        End If

        Return bMapped
    End Function

End Module
